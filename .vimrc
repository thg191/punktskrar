"
" ~/.vimrc
"

"Almennt {{{

filetype plugin indent on                           "Hleður inndráttsreglum og plöggum byggt á skráartýpum
syntax enable			                            "Sýnir liti byggt á forritunartungumáli

set showtabline=2
set nocompatible                                    "Til að slökkva á compatibility við gömul kerfi, það skemmir fyrir nýrri kerfum (eftir 1990 eða eitthvað fáránlegt)
set tabstop=2			                            "Setur inn 4 bil þegar <TAB> kemur fyrir.
set softtabstop=2		                            "Setur inn 4 bil þegar ýtt er á <TAB>
set shiftwidth=2                                    "Þetta er til að stilla sjálfvirkan inndráttinn
set expandtab			                            "Breytir <TAB> í bókstafleg bil
set number			                                "Sýnir línunúmer á vinstri hlið
set wildmenu			                            "Uppástungur fyrir gamlar skipanir
set lazyredraw			                            "Endurhleður skjáinn ekki jafn oft, hraðari skipanir
set number relativenumber	                        "Stillir á afstæða númeringu, þ.e. að númer núverandi línu er í miðjunni og svo er talið upp og niður
set autoindent			                            "Sjálfvirkur inndráttur
set breakindent                                     "Viðheldur inndrætti á brotnum línum
set ruler			                                "Sýnir línu og dálk í neðra hægra horninu

" Möppusýn
let g:netrw_liststyle = 3                           "Sýnir möppur sem tré
let g:netrw_banner = 0                              "Sýnir ekki hausinn
let g:netrw_browse_split = 3                        "Opnar skrár í nýjum flipa (1-landslag, 2-lóðrétt, 3-flipi, 4-fyrri gluggi)
let g:netrw_winsize = 20                            "Breidd netrw dálksins
let g:netrw_keepdir = 0                             "Kemur í veg fyrir bögg þegar skrár eru færðar
let g:netrw_list_hide = '\(^\|\s\s\)/zfs\.\S\+'     "Sýnum ekki faldar skrár
let g:netrw_mousemaps = 0                           "Slökkvum á músinni
let g:netrs_sizestyle = "H"                         "Sýnum stærðir sem K, M og G í 1024 grunni
let g:netrw_preview = 1                             "Preview sýnist sem vsplit til hægri

" Stillingar fyrir statuslínuna
set laststatus=2                    "Sýnir alltaf status línu
set statusline=
set statusline+=%1*[%f]
set statusline+=%{FugitiveStatusline()}
set statusline+=[%p%%]
set statusline+=\ %{FileSize()}\ %*
set statusline+=%2*%=%*
set statusline+=%#warningmsg#
set statusline+=%*
set statusline+=%3*\ %l,%c\ [%L]
set statusline+=\ %{&fileformat}
set statusline+=\ %{(&fenc!=''?&fenc:&enc)}%*

" litir
hi User1 ctermfg=0 ctermbg=6 cterm=bold
hi User2 ctermfg=0 ctermbg=8 cterm=bold
hi User3 ctermfg=0 ctermbg=6 cterm=bold

" Litir f. flipalínu
hi Tabline ctermfg=255 ctermbg=238 cterm=NONE
hi TablineFill ctermfg=255 ctermbg=238 cterm=NONE

"Drekkjum okkur ekki í swap skrám
"Býr til ~ skrár sem eru backup skrár ef maður hættir án þess að vista
set backup
"Setjum ~ og .swp skrár í ~/.vim/tmp eða vinnumöppu ef hitt er ekki til,
"pirrandi að hafa milljón aukaskrár í möppunni sinni
set backupdir=~/.vim/tmp,. 
set directory=~/.vim/tmp,.
set writebackup

set backspace=indent,eol,start      "Því backspace er skrýtið
" }}}

"Leitarvél {{{

set showmatch			"Lýsir upp samsvarandi sviga

set incsearch			"Leitar meðan maður skrifar
set hlsearch			"Lýsir niðurstöður

"}}}

"Samanbrot {{{

set foldenable 			    "Kveikir á samanbroti

set foldlevelstart=10 		"Opnar flest brot

set foldnestmax=10		    "10 hreiðruð samanbrot max

set foldmethod=indent		"Brjóta samkvæmt inndrætti

"}}}

"Kortlagningar {{{
" Þetta eru stillingar á flýtilyklum
let mapleader = ","                             "Þetta er leader takkinn, það er bara modifier, mér finnst komma þægilegust

"Slekkur á niðurstöðulýsingu. 
nnoremap <leader><space> :nohlsearch<CR>

"space opnar/lokar brotum
nnoremap <space> zA

"Færsla í insert ham                            "Þetta er til þess að maður geti verið í insert mode og fært sig með vim lyklunum á meðan maður heldur inni control (hér er þægilegt að vera með caps sem control)          
inoremap <C-j> <C-o>gj
inoremap <C-k> <C-o>gk
inoremap <C-h> <Left>
inoremap <C-l> <Right>

"Flýtileið til að opna .vimrc skrána, þetta er bókstaflega ,ev í normal mode, það opnar þessa skrá í nýjum flipa   
nnoremap <leader>ev :tabedit $VIMRC<CR>       

"Flýtileið til að source-a .vimrc skrána
nnoremap <leader>sv :source $VIMRC<CR>

" Flýtileið til að opna möppusýn
nnoremap <leader>f :Files<CR>

" Flýtileið til að opna skrár í flipa í möppusýn
nnoremap <leader>t :tabedit<space>

"Afrita og líma
vnoremap <C-c> "+y
"noremap <C-v> "+p
inoremap <C-p> <esc>pa

"Færum okkur niður um línu þó að neðri línan sé brotin.
nnoremap j gj
nnoremap k gk

"Færsla til Byrjunar/Enda línu
nnoremap B ^
nnoremap E $
nnoremap $ <nop>
nnoremap ^ <nop>

" Til þæginda, jk kemur í staðinn fyrir escape, ef maður þarf að skrifa jk
" þarf bara að skrifa það hægt
inoremap jk <esc>
nnoremap <leader>w :w<CR>
  
"Bindingar til að umlykja orð með hlutum
" ," umlykur orð með " o.s.frv.
nnoremap <leader>" viw<esc>a"bi"<esc>lel
nnoremap <leader>{ viw<esc>a{bi}<esc>lel
nnoremap <leader>( viw<esc>a(bi)<esc>lel

"Flýtileið til að miðjusetja bendilinn
inoremap <C-f> <esc>zzi 

"Flýtileið til að eyða innihaldi sviga
onoremap in( :<c-u>normal! f(vi(<CR>
onoremap il( :<c-u>normal! F)vi(<CR>
onoremap in{ :<c-u>normal! f{vi{<CR>
onoremap il{ :<c-u>normal! F}vi{<CR>

"Flýtileið til að leita að orði undir bendli í skrám
nnoremap <leader>g :Rg '<cWORD>'<CR>

"Færsla milli flipa með Ctrl + <nr>
nnoremap <c-l> gt<CR>
nnoremap <c-h> gT<CR>

"}}}

"Viðbætur {{{

" Bætum fzf í runtimepath:
set rtp+=/usr/bin/fzf

" Hlöðum viðbótum, reynum að halda þessu einföldu
call plug#begin('~/.vim/plugged')
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'glench/vim-jinja2-syntax'
call plug#end()

" Viðbótarstillingar
inoremap <expr> <c-x><c-f> fzf#vim#complete("find -print0 <Bar> xargs --null realpath --relative-to " . expand("%:h"))

"}}}

"Autocommands og sérstakar skráastillingar {{{

"Almennar stillingar
augroup configgroup
  autocmd!
  autocmd InsertLeave * if expand('%:t') !~ 'calendar.txt\|journal.txt'| set relativenumber | endif
  autocmd InsertEnter * set norelativenumber
augroup end

"Stillingar varðandi tex skrár
"Stillum á latex en ekki plaintex
let g:tex_flavor='latex'
let g:tex_indent_items=0
augroup filetype_tex
  autocmd!
  autocmd Filetype tex setlocal foldmethod=marker
  autocmd Filetype tex setlocal foldlevel=10
  autocmd Filetype tex setlocal sw=2
  autocmd Filetype tex setlocal softtabstop=2 
  autocmd Filetype tex setlocal foldmarker=???,???
  autocmd Filetype tex nnoremap <leader>b :w<CR> :! pdflatex --shell-escape -output-directory %:p:h %<CR><CR> 
  autocmd Filetype tex nnoremap <leader>o :! okular $(echo % \| sed -e 's/tex$/pdf/') & disown<CR><CR>
  autocmd Filetype tex nnoremap <leader>$ F\i$<ESC>2ea$
augroup end

augroup calendar
  autocmd!
  autocmd BufRead calendar.txt,journal.txt setlocal nonumber
  autocmd BufRead calendar.txt,journal.txt setlocal norelativenumber
  autocmd BufRead calendar.txt,journal.txt syntax match Date /^[0-9]\{4}-[0-9]\{2}-[0-9]\{2}/
  autocmd BufRead calendar.txt,journal.txt syntax match Week /w[0-9]\{2}/
  autocmd BufRead calendar.txt,journal.txt syntax match Day /Mán\|Þri\|Mið\|Fim\|Fös\|Lau\|Sun/
  autocmd BufRead calendar.txt,journal.txt highlight Date ctermfg=yellow
  autocmd BufRead calendar.txt,journal.txt highlight Week ctermfg=5
  autocmd BufRead calendar.txt,journal.txt highlight Day ctermfg=cyan
augroup end
  
augroup filetype_java
    autocmd!
    autocmd Filetype java nnoremap <leader>b :w<CR> :! javac %<CR><CR>
augroup end

augroup filetype_python
    autocmd!
    autocmd Filetype python setlocal softtabstop=2
augroup end

augroup filetype_yaml
    autocmd!
    autocmd Filetype yaml setlocal tabstop=2
    autocmd Filetype yaml setlocal softtabstop=2
    autocmd Filetype yaml setlocal shiftwidth=2
    autocmd Filetype yaml setlocal expandtab
augroup end

augroup filetype_c
    autocmd!
    autocmd Filetype c setlocal tabstop=4
    autocmd Filetype c setlocal softtabstop=4
    autocmd Filetype c setlocal shiftwidth=4
    autocmd Filetype c setlocal expandtab
augroup end

augroup filetype_xml
    autocmd!
    autocmd Filetype xml setlocal tabstop=4
    autocmd Filetype xml setlocal softtabstop=4
    autocmd Filetype xml setlocal shiftwidth=4
    autocmd Filetype xml setlocal expandtab
augroup end

augroup filetype_j2
    autocmd!
    autocmd Filetype j2 setlocal tabstop=4
    autocmd Filetype j2 setlocal softtabstop=4
    autocmd Filetype j2 setlocal shiftwidth=4
    autocmd Filetype j2 setlocal expandtab
augroup end

augroup lineColor
  autocmd!
  autocmd WinEnter,vimEnter * highlight lineNr ctermfg=yellow
  autocmd WinLeave * highlight lineNr ctermfg=black
augroup END

" Fortran stillingar
let fortran_free_source=1
let fortran_do_enddo=1

" Terraform stillingar
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1

"}}}

"Föll {{{

" Fall til að skilgreina hvernig flipar líta út
function MyTabLine()
  let s = ''
  for i in range(tabpagenr('$'))
    " Til að finna hvaða flipi er virkur
    if i + 1 == tabpagenr()
      let s ..= '%#TabLineSel#'
    else
      let s ..= '%#TabLine#'
    endif
    " tölumerkjum flipa, held ég
    let s ..= '%' .. (i + 1) .. 'T'
    " Köllum á MyTabLabel sem býr til merkinguna
    let s ..= ' %{MyTabLabel(' .. (i + 1) .. ')} '
  endfor

  " Litum bilið eftir seinasta flipann
  let s ..= '%#TabLineFill#%T'

  " Hægrijöfnum 
  if tabpagenr('$') > 1
    let s ..= '%=%#TabLine#%999Xclose'
  endif

  return s
endfunction

" Fall sem sækir númer flipa
function MyTabLabel(n)
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  return bufname(buflist[winnr - 1])
endfunction








function! Latex()
  read ~/.vim/templates/HDtemplate.tex
endfunction

function! FileSize() abort
    let l:bytes = getfsize(expand('%p'))
    if (l:bytes >= 1024)
        let l:kbytes = l:bytes / 1025
    endif
    if (exists('kbytes') && l:kbytes >= 1000)
        let l:mbytes = l:kbytes / 1000
    endif

    if l:bytes <= 0
        return '0'
    endif

    if (exists('mbytes'))
        return l:mbytes .'MB '
    elseif (exists('kbytes'))
        return l:kbytes . 'KB '
    else
        return l:bytes . 'B '
    endif
endfunction

" }}}
" vim:foldmethod=marker:foldlevel=0:ts=4:sts=4
                   
