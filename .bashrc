#
# ~/.bashrc
#

#
# Almennar stillingar
#


# Athugar stærð glugga eftir hverja skipun og uppfærir fjölda raða og dálka eftir þörfum
shopt -s checkwinsize
# Heldur áfram að skrifa í skrána sem geymir gamlar skipanir, í stað þess að gera nýja
shopt -s histappend

# Hlöðum eftirfarandi skrám ef þær eru lesanlegar
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
# Git status í skipanaprompti
[ -r ~/.git/git-completion.bash ] && . ~/.git/git-completion.bash
export GIT_PS1_SHOWDIRTYSTATE=1
[ -r ~/.git/git-prompt.sh ] && . ~/.git/git-prompt.sh

# Hlöðum inn litum frá ~/.dir_colors
eval $(dircolors -b ~/.dir_colors)

# Skipanakvaðning með git upplýsingum ef þær eru tiltækar
PS1='\[\e[01;32m\][\u@\h\[\e[01;37m\] \W$(__git_ps1 " (\[\e[01;32m\]%s\[\e[01;37m\])")\[\e[01;32m\]]\$\[\e[00m\] '

#
# Alias og umhverfisbreytur
#

# Fyrir ssh-agent púkann í .config/systemd/user/
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# GPG stillingar
export GPGKEY=877E4F92BD2D5C9F

# Ýmsar flýtileiðir fyrir skrár
export cal=$HOME/Documents/calendar/calendar.txt
export journal=$HOME/Documents/calendar/journal.txt
export VIMRC=$HOME/.vimrc
export BASHRC=$HOME/.bashrc
# Leitum að keyranlegum skrám í þessum möppum
export PATH=$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/var/lib/snapd/snap/bin:/usr/local/go/bin

export QT_STYLE_OVERRIDE=kvantum
export EDITOR=vim

alias ls='ls --color=auto --group-directories-first'                            # ls-um með litum, og hópum möppur saman!
alias grep='grep --colour=auto'                                                 # grep-um með litum líka!
alias cp="cp -i"                                                                # spyrja áður en eitthvað er yfirskrifað
alias free='free -m'                                                            # sýnum stærðir í MB
alias clc='clear'                                                               # eins og ég vandist í matlab
alias punktur='/usr/bin/git --git-dir=$HOME/.punktar/ --work-tree=$HOME'        # skipun til að halda utan um punktskrár í git
alias agent='eval `ssh-agent`'                                                  # keyrir ssh-agent
alias keyringer='$HOME/Projects/isnic/keyringer/keyringer'                      # lykilorðageymsla isnic
alias frisbivedur='ruby $HOME/Projects/thg/frisbivedur/frisbivedur.rb'          # frisbíveðurspá
alias caledit='vim +/$(date +%Y-%m-%d).\\{8,\} +nohl $cal'                      # opnar dagatalsskrá á línu núverandi dagsetningar
alias journal='vim +/$(date +%Y-%m-%d).\\{8,\} +nohl $journal'                  # sama nema dagbók
alias vika='grep $(date +%Y).*w$(date +%W) $cal | while read line; do  if [[ $line == "$(date +%Y-%m-%d)"* ]]; then echo -e "\033[0;36m$line\033[0;37m"; else echo -e "\033[0;37m$line\033[0;37m"; fi; done'

# Einskonar MOTD, listar dagatalsatburði og tiltækar uppfærslur
$HOME/.local/bin/vika
